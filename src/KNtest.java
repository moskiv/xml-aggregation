package com;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.SQLOutput;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class KNtest {

    // What is implemented:
    // 1. Scan properties file, output file and input file Paths
    // 2. Read properties file and store it in hash map as a key value.
    // 3. Find elements according to example properties file
    // 4. Change XML values according to example properties
    // 5. Create output XML file with updated values

    public static void main(String[] args) throws Exception {

        System.out.println("This is a XML file modifier based on XML input and property file.");
        System.out.println("Please set Property file path.");
        Scanner propertyScanner = new Scanner(System.in);
        String filePath = propertyScanner.nextLine();
        System.out.println("property file path selected: " + filePath);

        System.out.println("Please set XML input file path.");
        Scanner inScanner = new Scanner(System.in);
        String inputXML = inScanner.nextLine();
        System.out.println("property file path selected: " + inputXML);

        System.out.println("Please set output XML file path.");
        Scanner outScanner = new Scanner(System.in);
        String outputXML = outScanner.nextLine();
        System.out.println("property file path selected: " + filePath);


//        String filePath = "C:\\KN\\examples.properties";
//        String inputXML = "C:\\KN\\input.xml";
//        String outputXML = "C:\\KN\\outputs.xml";

        // Reading Property file and storing it as a key value sequence
        HashMap<String, String> keyValue = new HashMap<String, String>();

        String line;
        //Here I store key-value to hashmap and remove quotes so I could compare keys to nodes textContent
        BufferedReader reader = new BufferedReader(new FileReader(filePath));
        while ((line = reader.readLine()) != null)
        {
            String[] parts = line.split("=", 2);
            if (parts.length >= 2)
            {
                String key = parts[0];
                String newKey = key.replace("\"" , "");
                String value = parts[1];
                String newValue = value.replace("\"", "");
                keyValue.put(newKey, newValue);
            }
        }

        // Here we loop through the list and print out all keys and its values. This is just for a check up.
        for (String key : keyValue.keySet())
        {
           // System.out.println(key + " " + keyValue.get(key));
        }
        reader.close();

        // Here Im making list of only KEYS so later we can compare them with XML node elements
        List<String> keyList = new ArrayList<String>(keyValue.keySet());

        // 1. Read elements from inputXML file
        File inputXml = new File(inputXML);

        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = dbFactory.newDocumentBuilder();
        Document document = docBuilder.parse(inputXml);

        Node generalList = document.getElementsByTagName("CTRL_SEG").item(0);

        // As se have only 1 general node, we need to loop through inside "CTRL_SEG" tag for other elements
        NodeList list = generalList.getChildNodes();

        //All XML children element iteration
        for (int i = 0; i < list.getLength(); i++) {

            Node node = list.item(i);
            NodeList insideList = node.getChildNodes(); // loop inside second element to get child nodes
            for (int j = 0; j < insideList.getLength(); j++) {

                // Comparing keys form hashmap to nodes text, if key matches nodes textContent then update textContent with key's value.
                for (String key:keyList ) {
                    if ( node.getTextContent().contains(key) ) {
                        String value = keyValue.get(key);

                        // Check if value contains "TIME" character sequence - to determine that we are dealing with timestamp and assign timestamp
                        if (value.contains("TIME")){
                            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                            Date date = new Date();
                            value = dateFormat.format(date);
                        }
                        // Check if value starts with "<" to determine whether we need to programmatically assign unigue ID
                            if (value.startsWith("<") || value.startsWith("&lt;") ){
                                value = UUID.randomUUID().toString();
                            }
                        // Assign all key values to corresponding nodes
                        node.setTextContent(value);

                    }
                }
            }
        }

        // 5. Create output XML file with updated values
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(document);

            StreamResult streamResult = new StreamResult(new File(outputXML));

            transformer.transform(source, streamResult);

        System.out.println( "Success, you can find out XML in: " + outputXML);
    }
}
